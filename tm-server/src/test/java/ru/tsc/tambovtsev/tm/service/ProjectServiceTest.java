package ru.tsc.tambovtsev.tm.service;


public final class ProjectServiceTest {
/*
    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    @Before
    public void setProjectService() {
        projectService.create("123", "432", "222");
        projectService.create("1321", "234", "555");
    }

    @After
    public void clearProjectService() {
        projectService.clear();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(projectService.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(projectService.findAll().get(0).getName().isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<Project> projects = projectService.findAll();
        @Nullable final String projectId = projects.stream().findFirst().get().getId();
        Assert.assertFalse(projectService.findById(projectId).getName().isEmpty());
    }

    @Test
    public void testRemove() {
        @NotNull final Project project = projectService.findAll().get(1);
        projectService.remove(project);
        Assert.assertNotEquals(project.getId(), projectService.findAll().get(0).getId());
    }

    @Test
    public void testRemoveById() {
        @NotNull final Project project = projectService.findAll().get(1);
        projectService.removeById(project.getId());
        Assert.assertNotEquals(project.getId(), projectService.findAll().get(0).getId());
    }

    @Test
    public void testExistById() {
        @NotNull final Project projectFirst = projectService.findAll().get(0);
        Assert.assertTrue(projectService.existsById(projectFirst.getId()));
    }

    @Test
    public void testUpdateById() {
        @NotNull final Project project = projectService.findAll().get(0);
        projectService.updateById(project.getUserId(), project.getId(), "Project1", "Descriptions");
        Assert.assertEquals(projectService.findById(project.getId()).getName(), "Project1");
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdNegative() {
        @NotNull final Project project = projectService.findAll().get(0);
        projectService.updateById(project.getUserId(), null, "Project1", "Descriptions");
        Assert.assertEquals(projectService.findById(project.getId()).getName(), "Project1");
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Project project = projectService.findAll().get(0);
        projectService.changeProjectStatusById(project.getUserId(), project.getId(), Status.COMPLETED);
        Assert.assertEquals(projectService.findById(project.getId()).getStatus(), Status.COMPLETED);
    }

    @Test
    public void testClear() {
        projectService.clear();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }
*/
}

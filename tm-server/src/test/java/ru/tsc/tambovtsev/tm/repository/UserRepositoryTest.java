package ru.tsc.tambovtsev.tm.repository;

public final class UserRepositoryTest {
/*
    private final IUserRepository userRepository = new UserRepository();

    @Before
    public void setUserRepository() {
        @NotNull User[] usersArray = {
                new User(
                "testUnit1",
                        "242342342342",
                        "qwerty@test.ru",
                        "First",
                        "Last"),
                new User(
                        "testUnit2",
                        "345342342342",
                        "qwerty2@test.ru",
                        "First2",
                        "Last2")
            };

        Arrays.stream(usersArray).forEach(userRepository::add);
    }

    @After
    public void clearUserRepository() {
        userRepository.clear();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(userRepository.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(userRepository.findAll().get(0).getId().isEmpty());
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User findUser =  userRepository.findByLogin("testUnit1");
        Assert.assertFalse(findUser.getLogin().isEmpty());
    }

    @Test
    public void testFindByEmail() {
        @Nullable final User findUser =  userRepository.findByEmail("qwerty@test.ru");
        Assert.assertFalse(findUser.getEmail().isEmpty());
    }

    @Test
    public void testRemoveById() {
        @Nullable final User findUser =  userRepository.findAll().get(0);
        userRepository.removeById(findUser.getId());
        Assert.assertNotEquals(userRepository.findAll().get(0).getId(), findUser.getId());
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final User findUser =  userRepository.findAll().get(0);
        userRepository.removeByLogin(findUser.getLogin());
        Assert.assertNotEquals(userRepository.findAll().get(0).getId(), findUser.getId());
    }

    @Test
    public void testClear() {
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }
*/
}

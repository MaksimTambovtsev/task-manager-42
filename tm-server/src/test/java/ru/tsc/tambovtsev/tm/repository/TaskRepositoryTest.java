package ru.tsc.tambovtsev.tm.repository;

public final class TaskRepositoryTest {

    /*private final ITaskRepository taskRepository = new TaskRepository();

    @Before
    public void setTaskRepository() {
        taskRepository.create("123", "432", "222");
        taskRepository.create("1321", "234", "555");
    }

    @After
    public void clearTaskRepository() {
        taskRepository.clear();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(taskRepository.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(taskRepository.findAll().get(0).getName().isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<Task> tasks = taskRepository.findAll();
        @Nullable final String taskId = tasks.stream().findFirst().get().getId();
        Assert.assertFalse(taskRepository.findById(taskId).getName().isEmpty());
    }

    @Test
    public void testAdd() {
        @NotNull final TaskRepository taskRepository1 = new TaskRepository();
        taskRepository1.add(taskRepository.findAll().get(1));
        @Nullable final Task taskExp = taskRepository.findAll().get(1);
        @Nullable final Task taskAct = taskRepository1.findAll().get(0);
        Assert.assertEquals(taskExp.getId(), taskAct.getId());
        Assert.assertEquals(taskExp.getName(), taskAct.getName());
        Assert.assertEquals(taskExp.getDescription(), taskAct.getDescription());
    }

    @Test
    public void testRemove() {
        @NotNull final Task task = taskRepository.findAll().get(1);
        taskRepository.remove(task);
        Assert.assertNotEquals(task.getId(), taskRepository.findAll().get(0).getId());
    }

    @Test
    public void testRemoveById() {
        @NotNull final Task task = taskRepository.findAll().get(1);
        taskRepository.removeById(task.getId());
        Assert.assertNotEquals(task.getId(), taskRepository.findAll().get(0).getId());
    }

    @Test
    public void testExistById() {
        @NotNull final Task taskFirst = taskRepository.findAll().get(0);
        Assert.assertTrue(taskRepository.existsById(taskFirst.getId()));
    }

    @Test
    public void testClear() {
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }
*/
}

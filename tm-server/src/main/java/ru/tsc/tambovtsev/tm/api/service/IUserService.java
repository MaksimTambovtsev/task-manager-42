package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import java.util.List;

public interface IUserService extends IService<UserDTO> {

    void create(@Nullable String login, @Nullable String password);

    void create(@Nullable String login, @Nullable String password, @Nullable String email);

    void create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    UserDTO setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    UserDTO updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    void clearUser();

    @Nullable
    List<UserDTO> findAll();

}

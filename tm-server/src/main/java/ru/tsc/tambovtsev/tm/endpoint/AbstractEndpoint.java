package ru.tsc.tambovtsev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.service.IDomainService;
import ru.tsc.tambovtsev.tm.api.service.IServiceLocator;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.system.AccessDeniedException;
import ru.tsc.tambovtsev.tm.dto.model.SessionDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    protected SessionDTO check(@Nullable AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    protected SessionDTO check(@Nullable AbstractUserRequest request, @Nullable Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final SessionDTO session = serviceLocator.getAuthService().validateToken(token);
        @NotNull final String userId = session.getUserId();
        @Nullable final UserDTO user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();
        if (roleUser != role) throw new AccessDeniedException();
        return session;
    }


    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

}

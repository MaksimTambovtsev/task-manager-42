package ru.tsc.tambovtsev.tm.dynamicsql.model;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;
import ru.tsc.tambovtsev.tm.enumerated.Status;

import java.sql.JDBCType;

public class ProjectTable extends SqlTable {

    public final SqlColumn<String> id = column("ID", JDBCType.VARCHAR);

    public final SqlColumn<String> userId = column("USER_ID", JDBCType.VARCHAR);

    public final SqlColumn<String> name = column("NAME", JDBCType.VARCHAR);

    public final SqlColumn<String> description = column("DESCRIPTION", JDBCType.VARCHAR);

    public final SqlColumn<Status> status = column("STATUS", JDBCType.VARCHAR);

    public ProjectTable() {
        super("TM_PROJECT");
    }

}

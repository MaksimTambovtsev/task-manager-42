package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class UserService extends AbstractService<UserDTO, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    public UserService(@NotNull IConnectionService connection,
                       @Nullable final IUserRepository userRepository,
                       @NotNull IPropertyService propertyService,
                       @Nullable IProjectRepository projectRepository,
                       @Nullable ITaskRepository taskRepository) {
        super(userRepository, connection);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = repository.findByLogin(login);
            if (user == null) return null;
            return user;
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = repository.findByEmail(email);
            if (user == null) return null;
            return user;
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.removeByLogin(login);
            sqlSession.commit();
        } catch (@NotNull final Exception e){sqlSession.rollback();}
        finally {
            sqlSession.close();
        }
        return null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = new UserDTO();
            user.setRole(Role.USUAL);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.create(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e){sqlSession.rollback();}
        finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = new UserDTO();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            repository.create(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void create(
            final String login,
            final String password,
            final Role role
    ) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = new UserDTO();
            user.setRole(role);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.create(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO setPassword(
            @Nullable final String userId,
            @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = repository.findById(userId);
            if (user == null) return null;
            final String hash = HashUtil.salt(propertyService, password);
            user.setPasswordHash(hash);
            repository.updateUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = repository.findById(userId);
            if (user == null) return null;
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.updateUser(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = findByLogin(login);
            repository.lockUserById(user.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO user = findByLogin(login);
            repository.unlockUserById(user.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public @NotNull IRepository<UserDTO> getRepository(@NotNull SqlSession connection) {
        return connection.getMapper(IUserRepository.class);
    }

    @Override
    public void addAll(@NotNull final Collection<UserDTO> models) {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clearUser() {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.clearUser();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final List<UserDTO> result = repository.findAllUser();
            return result;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @NotNull
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> models) {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.clearUser();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

}

package ru.tsc.tambovtsev.tm.dynamicsql.provider;

import org.mybatis.dynamic.sql.SqlColumn;
import ru.tsc.tambovtsev.tm.dynamicsql.model.ProjectTable;
import ru.tsc.tambovtsev.tm.enumerated.Status;

public class ProjectProvider {

    public static final ProjectTable project = new ProjectTable();

    public static final SqlColumn<String> id = project.id;

    public static final SqlColumn<String> userId = project.userId;

    public static final SqlColumn<String> name = project.name;

    public static final SqlColumn<String> description = project.description;

    public static final SqlColumn<Status> status = project.status;

}

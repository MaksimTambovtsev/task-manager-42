package ru.tsc.tambovtsev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.tambovtsev.tm.handler.StatusTypeHandler;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<TaskDTO> {

    @Insert("INSERT INTO TM_TASK (ID, NAME, DESCRIPTION, STATUS, USER_ID) VALUES " +
            "(#{id}, #{name}, #{description}, #{status}, #{userId})"
    )
    void addAll(@NotNull Collection<TaskDTO> models);

    @Insert("INSERT INTO TM_TASK (ID, NAME, DESCRIPTION, STATUS, USER_ID) VALUES " +
            "(#{id}, #{name}, #{description}, #{status}, #{userId})"
    )
    void create(@NotNull TaskDTO task);

    @Update(
            "UPDATE TM_TASK SET " +
                    "NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status}, " +
                    "USER_ID = #{userId}, PROJECT_ID = #{projectId} WHERE ID = #{id}"
    )
    void updateById(@NotNull TaskDTO task);

    @Delete("DELETE FROM TM_TASK WHERE USER_ID = #{userId} AND PROJECT_ID = #{projectId}")
    void removeByProjectId(@Param ("userId") @NotNull String userId, @Param ("projectId") @NotNull String projectId);

    @Nullable
    @Select("SELECT * FROM TM_TASK WHERE ID = #{id} AND USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    TaskDTO findById(@Param ("userId") @Nullable String userId, @Param ("id") @Nullable String id);

    @Delete("DELETE FROM TM_TASK WHERE USER_ID = #{userId}")
    void clear(@Nullable String userId);

    @Nullable
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<TaskDTO> findAll(SelectStatementProvider selectStatement);

    @Nullable
    @Select("SELECT * FROM TM_TASK")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID")
    })
    List<TaskDTO> findAllTask();

    @Select("SELECT COUNT(1) FROM TM_TASK WHERE USER_ID = #{userId}")
    int getSize(@Nullable String userId);

    @Delete("DELETE FROM TM_TASK WHERE USER_ID = #{userId} AND ID = #{id}")
    void removeById(@Param ("userId") @Nullable String userId, @Param ("id") @Nullable String id);

    @Delete("DELETE FROM TM_TASK")
    void clearTask();

}

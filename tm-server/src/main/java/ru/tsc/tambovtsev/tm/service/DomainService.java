package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.repository.IDomainRepository;
import ru.tsc.tambovtsev.tm.api.service.IDomainService;

public class DomainService implements IDomainService {

    @NotNull
    IDomainRepository domainRepository;

    public DomainService(@NotNull final IDomainRepository domainRepository) {
        this.domainRepository = domainRepository;
    }

    @Override
    public void loadDataBackup() {
        domainRepository.loadDataBackup();
    }

    @Override
    public void saveDataBackup() {
        domainRepository.saveDataBackup();
    }

    @Override
    public void loadDataBase64() {
        domainRepository.loadDataBase64();
    }

    @Override
    public void saveDataBase64() {
        domainRepository.saveDataBase64();
    }

    @Override
    public void loadDataBinary() {
        domainRepository.loadDataBinary();
    }

    @Override
    public void saveDataBinary() {
        domainRepository.saveDataBinary();
    }

    @Override
    public void loadDataJsonFasterXml() {
        domainRepository.loadDataJsonFasterXml();
    }

    @Override
    public void saveDataJsonFasterXml() {
        domainRepository.saveDataJsonFasterXml();
    }

    @Override
    public void loadDataJsonJaxb() {
        domainRepository.loadDataJsonJaxb();
    }

    @Override
    public void saveDataJsonJaxb() {
        domainRepository.saveDataJsonJaxb();
    }

    @Override
    public void loadDataXmlJaxb() {
        domainRepository.loadDataXmlJaxb();
    }

    @Override
    public void saveDataXmlJaxb() {
        domainRepository.saveDataXmlJaxb();
    }

    @Override
    public void loadDataXmlFasterXml() {
        domainRepository.loadDataXmlFasterXml();
    }

    @Override
    public void saveDataXmlFasterXml() {
        domainRepository.saveDataXmlFasterXml();
    }

    @Override
    public void loadDataYamlFasterXml() {
        domainRepository.loadDataYamlFasterXml();
    }

    @Override
    public void saveDataYamlFasterXml() {
        domainRepository.saveDataYamlFasterXml();
    }

}

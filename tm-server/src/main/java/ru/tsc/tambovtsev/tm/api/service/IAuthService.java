package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.SessionDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

public interface IAuthService {

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

}


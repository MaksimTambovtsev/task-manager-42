package ru.tsc.tambovtsev.tm.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.VARCHAR)
public class RoleTypeHandler extends BaseTypeHandler<Role> {

    @Override
    public void setNonNullParameter(
            PreparedStatement ps,
            int i,
            Role parameter,
            JdbcType jdbcType
    ) throws SQLException {
        ps.setString(i, parameter.name());
    }

    @Override
    public Role getNullableResult(ResultSet rs, String columnName)
            throws SQLException {
        String val = rs.getString(columnName);
        if (rs.wasNull())
            return null;
        else
            return Role.valueOf(val);
    }

    @Override
    public Role getNullableResult(ResultSet rs, int columnIndex)
            throws SQLException {
        String val = rs.getString(columnIndex);
        if (rs.wasNull())
            return null;
        else
            return Role.valueOf(val);
    }

    @Override
    public Role getNullableResult(CallableStatement cs, int columnIndex)
            throws SQLException {
        String val = cs.getString(columnIndex);
        if (cs.wasNull())
            return null;
        else
            return Role.valueOf(val);
    }

}

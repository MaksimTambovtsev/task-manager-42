package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

}

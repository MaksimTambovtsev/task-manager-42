package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;

import java.util.Collection;

public interface IService<M extends AbstractEntityDTO> extends IRepository<M> {

    @Nullable
    M findById(@Nullable String id);

    void removeById(@Nullable String id);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

}


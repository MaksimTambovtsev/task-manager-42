package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.UserLoginResponse;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();
    private ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();
    private String token = null;
    private String idTask = null;

    @Before
    public void createTask() {
        final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        token = userLoginResponse.getToken();
        taskEndpoint.createTask(new TaskCreateRequest(
                token, "Task1", "Desc1")
        );
        idTask = taskEndpoint.listTask(new TaskListRequest(token, null))
                .getTasks().get(0).getId();
    }

    @Test
    public void createTaskTest() {
        taskEndpoint.createTask(new TaskCreateRequest(
                token, "Task2", "Desc2")
        );
        Assert.assertFalse(taskEndpoint.listTask(
                new TaskListRequest(token, null)).getTasks().get(1).getName().isEmpty()
        );
    }

    @Test
    public void changeTaskStatusByIdTest() {
        @NotNull final Status statusBefore = taskEndpoint.listTask(new TaskListRequest(token, null))
                .getTasks().get(0).getStatus();
        taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(token, idTask, Status.COMPLETED));
        Assert.assertNotEquals(statusBefore, taskEndpoint.listTask(new TaskListRequest(token, null)).
                getTasks().get(0).getStatus()
        );
    }

    @Test
    public void removeTaskByIdTest() {
        taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, idTask));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(token, null)).
                getTasks()
        );
    }

    @Test
    public void updateTaskByIdTest() {
        @NotNull final String nameBefore = taskEndpoint.listTask(new TaskListRequest(token, null))
                .getTasks().get(0).getName();
        taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(token, idTask, "qwerty", "23234"));
        Assert.assertNotEquals(nameBefore, taskEndpoint.listTask(new TaskListRequest(token, null)).
                getTasks().get(0).getName()
        );
    }

    @Test
    public void showTaskTest() {
        Assert.assertNotNull(taskEndpoint.showTask(
                new TaskShowByIdRequest(token, idTask)).getTask().getName()
        );
    }

    @Test
    public void listTaskTest() {
        Assert.assertNotNull(taskEndpoint.listTask(
                new TaskListRequest(token, null)).getTasks()
        );
    }



}

package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.ICommandRepository;
import ru.tsc.tambovtsev.tm.api.service.ICommandService;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @Nullable
    private final ICommandRepository commandRepository;

    public CommandService(@Nullable ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }
}

package ru.tsc.tambovtsev.tm.command.domain;

import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.Domain;
import ru.tsc.tambovtsev.tm.dto.request.DataJsonSaveJaxBRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DomainSaveJsonJaxbCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "save-json-jaxb";

    @NotNull
    private final static String DESCRIPTION = "Save projects, tasks and users in json file";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataJsonJaxB(new DataJsonSaveJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

package ru.tsc.tambovtsev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es =
            Executors.newSingleThreadScheduledExecutor();

    private Bootstrap bootstrap;

    private final List<String> commands = new ArrayList<>();

    private final File folder = new File("./");

    public FileScanner(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        final Iterable<AbstractCommand> commands =
                bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(c -> this.commands.add(c.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file: folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            final boolean checkFile = commands.contains(fileName);
            if (checkFile) {
                file.delete();
                bootstrap.processCommand(fileName);
            }
        }
    }

    public void start() {
        init();
    }

}

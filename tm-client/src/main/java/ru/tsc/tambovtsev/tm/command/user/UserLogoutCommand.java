package ru.tsc.tambovtsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.request.UserLogoutRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-logout";

    @NotNull
    public static final String DESCRIPTION = "Logout user profile.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getServiceLocator().getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

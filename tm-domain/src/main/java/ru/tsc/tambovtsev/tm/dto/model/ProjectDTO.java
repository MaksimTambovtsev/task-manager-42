package ru.tsc.tambovtsev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.model.IWBS;
import ru.tsc.tambovtsev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TM_PROJECT")
public class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "NAME")
    private String name = "";

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description = "";

    @Nullable
    @Column(name = "CREATED")
    private Date created = new Date();

    @NotNull
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + getStatus().getDisplayName() + " : " + description;
    }

}
